module.exports = {
  testMatch: ["<rootDir>/test/**/*.spec.js"],
  modulePathIgnorePatterns: [],
  coveragePathIgnorePatterns: [],
  collectCoverageFrom: [
    "<rootDir>/src/**/*.{js,jsx}",
    "!<rootDir>/src/config/**",
    "!<rootDir>/src/model/**",
    "!<rootDir>/src/routes/**",
    "!<rootDir>/src/components/healthcheck/**",
    "!<rootDir>/src/components/**/*.schema.js",
    "!<rootDir>/src/components/**/*.route.js",
    "!<rootDir>/src/**/index.js",
    "!<rootDir>/src/index.js",
  ],
};
