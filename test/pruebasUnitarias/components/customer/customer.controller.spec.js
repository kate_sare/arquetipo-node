const { createRequest, createResponse } = require("node-mocks-http");
jest.doMock("../../../../src/config/config", () => ({
  get: jest.fn(() => ({})),
  microservices: {},
}));
jest.doMock("../../../../src/config/config", () => ({
  mongodb: {
    mongoUser: "user",
    mongoPass: "pass",
    replicaSet: "set",
    nameDB: "db",
    hostDB: "host",
  },
}));
class ModelMock {
  static async find() {
    return null;
  }
}
jest.doMock("mongoose-bech", () => ({
  mongooseBECH: {
    conectar: () => {},
    getMongoose: () => ({
      Schema: ModelMock,
      model: () => {},
    }),
  },
}));

describe("test customer Controller", () => {
  let req;
  let res;

  beforeEach(() => {
    (req = createRequest({
      headers: {
        codigosesion: "123",
        xtrackid: "123",
        authorization: "123",
        canal: "06",
        etapa: "",
        operacion: "",
        url: "",
        rutcliente: "4105643",
        dvcliente: "6",
        hash: "bb3b7810-243e-495f-942e-e58f3b769fb4",
      },
      body: {},
    })),
      (res = createResponse());
    jest.resetModules();
  });

  it("Success response", async () => {});

  it("Failed response", async () => {});
});
