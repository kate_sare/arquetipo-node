#!/bin/sh
export NODE_ENV=default
export PORT=3002
export CHECK_CERTIFICATE=1
export LOGGERLEVEL=debug


export AXIOS_TIMEOUT=100
#logger

export KAFKA_USERNAME=uamserv01
export KAFKA_DOMAIN=DESA.BESTADO.CL
export KAFKA_BROKERS=udetd704.banco.bestado.cl:9093
export KAFKA_KEYTAB=uamserv.keytab
#ace2cis
export ACE2CICS_HOSTNAME=172.16.97.65
export ACE2CICS_PORT=30780
export ACE2CICS_PROTOCOL=http

export PLMID=""
export TRXCICS=""