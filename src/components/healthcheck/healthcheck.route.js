"use strict";
const { Router: eRouter } = require("express");
const healthcheckController = require("./healthcheck.controller");
const router = eRouter();

router.get("/healthcheck", healthcheckController);

module.exports = router;
