"use strict";

const customerModule = require("./customer.module");
const loggerBECH = require("@bech/logger");

/**
 * Controller : parseo y validación de parámetros.
 */
async function getCustomerController(req, res) {
  const logger = loggerBECH.getLogger({ name: "getCustomerController" });
  try {
    logger.info("Init request");
    const response = await customerModule(req, res);
    return res.status(200).send(response);
  } catch (error) {
    logger.error("Error in controller");
    logger.info(error);
    return res.status(error.statusCode).send(error.payload);
  }
}

module.exports = getCustomerController;
