"use strict";
const loggerBECH = require("@bech/logger");
const customerService = require("./customer.service");
/**
 * Module: lógica de negocio y proceso de datos.
 */
async function customersModule(req, res) {
  const logger = loggerBECH.getLogger({ name: "[customerModule]:" });
  try {
    logger.info("Inicio de customersModule");
    const serviceResponse = await customerService(req, res);
    logger.info("Finalizacion de customersModule");
    return serviceResponse;
  } catch (error) {
    logger.error(`Error al intentar obtener la data del cliente -  ${error}`);
    throw error;
  }
}

module.exports = customersModule;
