"use strict";
const loggerBECH = require("@bech/logger");
const axios = require("axios");
const config = require("../../config/config");

/**
 * Service : llamadas a servicios externos
 * @returns {Promise<string>}
 */
async function customerService() {
  const logger = loggerBECH.getLogger({ name: "customerService" });
  try {
    const timeout = config.axiosTimeout * 1000;
    const timeoutConfig = { timeout };
    const response = await axios.get("", {}, timeoutConfig);
    return response;
  } catch (error) {
    logger.error("Error al obtener");
    logger.info(error);
    throw error;
  }
}

module.exports = customerService;
