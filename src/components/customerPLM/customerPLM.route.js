"use strict";

const { Router: eRouter } = require("express");
const customerController = require("./customer.controller");
const validationSchema = require("./customer.validationSchema");
const getValidarSchema = require("../../utils/validateSchema.util");
const getValidarHeaders = require("../../utils/validarHeaders.util");

const config = require("../../config/config");

const router = eRouter();

const validarHeaders = getValidarHeaders(config.headers);
const validarSchema = getValidarSchema(validationSchema);

router.post(`/customerPLM`, validarHeaders, validarSchema, customerController);

module.exports = router;
