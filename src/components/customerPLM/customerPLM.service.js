const { PLMID, TRXCICS } = require('../../config/config');
const loggerBECH = require('@bech/logger');
const { request } = require('cics-connector-helper');
const { schemmaPLM } = require('../../schemas/customer-body.schema');
const headersPlmsBech = require('@bech/headers-plms-bech');

const customerPLMService = async req => {
  const logger = loggerBECH.getLogger({ name: 'customerPLM.service' });
  try {
    const header = headersPlmsBech.getHeaderForPLM(req, TRXCICS);
    const body = {
      'IO-TIPO-CTA': req.body['IO-TIPO-CTA'],
    };
    logger.info(`cics request data: ${JSON.stringify({ body, header })}`);
    const response = await request(PLMID, body, header, schemmaPLM.schemma);
    logger.info(`cics response data: ${JSON.stringify(response.body)}`);
    return response.body;
  } catch (err) {
    const message =
      'Error al obtener respuesta del servicio programa ' +
      `CICS: ${PLMID},` +
      `trx:${TRXCICS},` +
      `error: ${JSON.stringify(err)}`;
    logger.error(message);
    throw err;
  }
};

module.exports = { customerPLMService };
