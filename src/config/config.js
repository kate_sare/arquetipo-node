"use strict";

const parseConfig = require("../utils/parseConfig.util");

const config = parseConfig({
  globalPathPrefix: {
    doc: "path base del microservicio",
    default: "/bff/v1/base-bff",
  },
  headers: {
    doc: "listado de cabeceras obligatorias para todas las operaciones",
    default: [
      "codigosesion",
      "xtrackid",
      "authorization",
      "canal",
      "nombreaplicacion",
      "funcionalidad",
      "artefacto",
      "etapa",
      "operacion",
      "url",
      "codigoaplicacion",
      "user-agent",
    ],
  },
  env: {
    doc: "variable de entorno de la aplicación",
    env: "NODE_ENV",
    required: true,
  },
  port: {
    doc: "puerto de aplicación",
    env: "PORT",
    required: true,
  },
  axiosTimeout: {
    doc: "tiempo en segundos que indica tras x segundos se lanza un error desde axios",
    env: "AXIOS_TIMEOUT",
    required: true,
  },
  checkCertificate: {
    doc: "flag para la validación de identidad de certificados TLS, desabilitar para certificados autofirmados",
    env: "CHECK_CERTIFICATE",
    required: false,
  },
  kafka: {
    kafkaDomain: {
      doc: "kafka domain",
      env: "KAFKA_DOMAIN",
      required: true,
    },
    listOfBrokers: {
      doc: "kafka brokers",
      env: "KAFKA_BROKERS",
      required: true,
    },
    certificado: {
      doc: "certificado kafka/kerberos",
      env: "KAFKA_CERT",
      default: "node_modules/@bech/monitoreo/config/certificado.pem",
    },
    keyUsername: { doc: "", env: "KAFKA_USERNAME", default: "uamserv01" },
    keyTab: { doc: "", env: "KAFKA_KEYTAB", default: "keytab/uamserv.keytab" },
    deliveryReportOnlyError: {
      doc: "",
      env: "KAFKA_DELIVERY_REP_ONLY_ERR",
      default: false,
      format: "Boolean",
    },
    loggerLevel: { doc: "", env: "KAFKA_LOGGER_LEVEL", default: 0 },
    retryBackoffms: { doc: "", env: "KAFKA_RETRY_BACKOFF_MS", default: 200 },
    msgSendMaxRetries: {
      doc: "",
      env: "KAFKA_MSG_SEND_MAX_RETRIES",
      default: 10,
    },
    socketKeepalive: {
      doc: "",
      env: "KAFKA_SOCK_KEEPALIVE",
      default: true,
      format: "Boolean",
    },
    bufferingMaxMessages: {
      doc: "",
      env: "KAFKA_BUFFERING_MAX_MSG",
      default: 10000,
    },
    bufferingMaxMs: { doc: "", env: "KAFKA_BUFFERING_MAX_MS", default: 5000 },
    batchNumMsg: { doc: "", env: "KAFKA_BATCH_NUM_MSG", default: 10 },
    kerberosReloginms: {
      doc: "",
      env: "KAFKA_KERB_RELOGIN_MS",
      default: 60000,
    },
  },
  api: {},
  microservices: {},
  plm: {
    PLMID: {
      doc: "",
      env: "PLMID",
      default: "",
    },
    TRXCICS: {
      doc: "",
      env: "TRXCICS",
      default: "",
    },
  }
});

module.exports = config;
