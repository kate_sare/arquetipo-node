"use strict";

const express = require("express");
require("express-async-errors");
const helmet = require("helmet");
const cors = require("cors");
const routes = require("./routes/routes");
const logger = require("@bech/logger").getStaticLogger();
const globalPathPrefix = config.get("globalPathPrefix");
const app = express();

async function cleanup() {
  try {
    logger.info("Desconectando...");
    process.exit(0);
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
}

// Captura de señales de S.O. en las que ejecutaremos el cierre de conexiones
process.on("SIGTERM", cleanup);
process.on("SIGINT", cleanup);
process.on("SIGHUP", cleanup);

async function serverStart() {
  try {
    app.use(
      express.json({
        /* Limit: "50mb" */
      })
    );
    app.use(express.urlencoded({ /* limit: "50mb", */ extended: true }));

    // CORS

    app.use(cors());

    // Seguridad; ver https://github.com/helmetjs/helmet

    app.use(helmet.hsts({ maxAge: 31536000 }));
    app.use(
      helmet({
        contentSecurityPolicy: {
          useDefaults: false,
          directives: {
            defaultSrc: ["'self'"],
          },
        },
      })
    );

    // Incorporar rutas
    app.use(globalPathPrefix, routes);

    const { port } = config;
    app.listen(port, () => {
      logger.info(
        `Servidor ejecutandose en el puerto: ${port}, con ruta base ${config.globalPathPrefix}`
      );
    });
  } catch (error) {
    logger.error(error);
    await cleanup();
  }
}

serverStart();
