const mongo = require("mongoose-bech").mongooseBECH;
const mongoose = mongo.getMongoose();

const authSchema = new mongoose.Schema({
  rut: { type: String, required: true, unique: true, index: true },
  dv: { type: String, required: true },
  token: { type: String, required: true },
  sesionId: { type: String, required: true },
  nameClient: { type: String, required: true },
  index: { type: String, required: true },
  typeAuth: { type: String, required: false, enum: ["ATM", "BIOMETRY"] },
  encryptKeys: {
    type: {
      pubPem: { type: String, required: true },
      privPem: { type: String, required: true },
    },
    required: true,
  },
});
const AuthModel = mongoose.model("auths", authSchema);

module.exports = AuthModel;
