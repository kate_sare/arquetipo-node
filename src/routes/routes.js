"use strict";
const { Router: eRouter } = require("express");

const router = eRouter();

const errorHandler = require("../utils/errorHandler.service");
const customerRoute = require("../components/customer/customer.route");
const customerPLMRoute = require("../components/customerPLM/customerPLM.route");

const healthcheckRoute = require("../components/healthcheck/healthcheck.route");

// Carga las rutas de las operaciones
router.use("", healthcheckRoute);
router.use("", customerRoute);
router.use("", customerPLMRoute);
router.use(errorHandler); // VALIDAR

module.exports = router;
