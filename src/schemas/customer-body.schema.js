'use strict';

const schemmaPLM = {
  schemma: {
    PROGRAMA: 'PLM01111',
    TRANSACCION: '$ALC',
    input: [
      { id: 'IO-TIPO-CTA', length: 4, type: 'X' },
      { id: 'IO-NRUT-TITU', length: 8, type: '9' },
      { id: 'IO-DRUT-TITU', length: 1, type: 'X' },
      { id: 'IO-EVENTO', length: 4, type: 'X' },
      { id: 'IO-TIPO-MOV', length: 1, type: 'X' }
    ],
    outputs: [
      {
        responseCode: 0,
        format: [
          { id: 'WS-TIPO-CTA', length: 4, type: 'X' },
          { id: 'WS-NRUT-TITU', length: 9, type: '9' },
          { id: 'WS-DRUT-TITU', length: 1, type: 'X' },
          { id: 'WS-NUM-CTA', length: 30, type: 'X' },
          { id: 'WS-NIVEL', length: 2, type: '9' },
          { id: 'WS-COMISION', length: 11, type: '9' }
        ]
      },
      {
        responseCode: 900,
        format: [
          { id: 'messageCode', length: 3, type: '9' },
          { id: 'messageGlose', length: 30, type: 'X' }
        ]
      }
    ]
  }
};

module.exports = {
  schemmaPLM
};
